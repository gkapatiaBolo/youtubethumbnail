import os

import pyvips
from PIL import Image
from PIL.Image import ANTIALIAS


def generate_hindi_text_image(input_text):
    output_file = "hindi_text_image.png"
    hindi_text = u'' + f'{input_text}'

    max_w, max_h = 1200, 900
    image = pyvips.Image.text(hindi_text, width=max_w, height=max_h, font='Arial Unicode MS', dpi=600)
    image.write_to_file(output_file)


input_image = Image.open('thumbnail.jpg')
text = 'स्कूल में extra curricular activities में भाग लेना कितना महत्वपूर्ण है?'
input_width, input_height = input_image.size

img = input_image.resize((round((720 * 2 * input_width) / input_height), 720 * 2), ANTIALIAS)
img_width, img_height = img.size

if img_width > img_height:
    raise Exception(f'width:{img_width} can not be more than height:{img_height}!')
else:
    output = Image.new('RGB', (1280 * 2, 720 * 2), 'black')
    output.paste(img, (1280 * 2 - img_width, 0))

    generate_hindi_text_image(text)
    text_img = Image.open('hindi_text_image.png')

    output.paste(text_img, (275, 275))
    os.remove('hindi_text_image.png')

    output = output.resize((1280, 720), ANTIALIAS)

    output.save('output.jpg')
